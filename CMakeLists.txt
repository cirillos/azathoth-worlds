cmake_minimum_required(VERSION 3.0)

# project name init
project(azathoth-worlds
		VERSION 1.0
		DESCRIPTION "Azathoth Worlds"
		LANGUAGES C)

# setting default settings
set(DEFAULT_BUILD_TYPE "Release")
set(CMAKE_C_FLAGS "-std=c11 -Wall -Wextra")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)

# finding all headers and sources
file(GLOB_RECURSE SOURCES ${PROJECT_SOURCE_DIR}/src/*.c)
file(GLOB_RECURSE HEADERS ${PROJECT_SOURCE_DIR}/include/*.h)

# include directories
include_directories(${PROJECT_SOURCE_DIR}/include)
#include_directories(${PROJECT_SOURCE_DIR}/src)

# ---------------- ADDING EXTERNAL LIBRARIES ----------------
# Vita
set(VITA_LIBRARY vita)
add_subdirectory(${PROJECT_SOURCE_DIR}/lib/vita)

# GLFW
set(BUILD_SHARED_LIBS OFF)	# build static library instead
set(GLFW_BUILD_DOCS OFF)	# do not build documentation

add_subdirectory(${PROJECT_SOURCE_DIR}/lib/glfw)

# OpenGL
find_package(OpenGL REQUIRED)
include_directories(${OpenGL_INCLUDE_DIRS})
link_directories(${OpenGL_LIBRARY_DIRS})
add_definitions(${OpenGL_DEFINITIONS})

if(NOT OpenGL_FOUND)
	message(Error "OpenGL not found")
endif(NOT OpenGL_FOUND)

# -----------------------------------------------------------

# compiling the executable
add_executable(${PROJECT_NAME} ${SOURCES} ${HEADERS})

# add library include directories
target_include_directories(${PROJECT_NAME} 
	PUBLIC ${PROJECT_SOURCE_DIR}/lib/vita/include
	PUBLIC ${PROJECT_SOURCE_DIR}/lib/glfw/include
)

# add library link directories (where libxxx.a resides)
target_link_directories(${PROJECT_NAME} 
	PRIVATE ${PROJECT_SOURCE_DIR}/lib/vita/lib
	PRIVATE ${PROJECT_SOURCE_DIR}/build/lib/glfw/src
)

# link all libraries with the executable
target_link_libraries(${PROJECT_NAME} 
	${OPENGL_LIBRARIES} 
	glfw 
	${VITA_LIBRARY}
)




































