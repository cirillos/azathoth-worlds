#include "cgml/graphics/window.h"

static void cgml_window_framebuffer_size_callback(GLFWwindow* window, int w, int h);

GLFWwindow *cgml_window_create(const int w, const int h, const char *const t, void (*window_hints_init)()) {
	// initializing GLFW
	if(!glfwInit()) {
		fprintf(stderr, "Failed to initialize GLFW!\n");
		exit(1);
	}

	// set default window hints, otherwise user-defined
	if(window_hints_init == NULL) {
		// setting window hints
		glfwWindowHint(GLFW_SAMPLES, 4); // antialiasing
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

		// if working on MacOS
		#if __APPLE__
			glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, true);
		#endif

		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	} else {
		window_hints_init();
	}

	// create a window instance
	GLFWwindow *window = glfwCreateWindow(w, h, t, NULL, NULL);
	if(window == NULL) {
		fprintf(stderr, "Failed to create a GLFW window!\n");
		exit(1);
	}

	// create OpenGL context
	glfwMakeContextCurrent(window);

	// tie drawing area to OpenGL
	glViewport(0, 0, w, h);

	// set input mode
	glfwSetInputMode(window, GLFW_STICKY_KEYS, true);

	// callbacks
	glfwSetFramebufferSizeCallback(window, cgml_window_framebuffer_size_callback);

	return window;
}

void cgml_window_destroy(GLFWwindow *window) {
	// destroy the window
	glfwDestroyWindow(window);
	window = NULL;

	// quit glfw
	glfwTerminate();
}

void cgml_window_clear(const color_t c) {
	glClearColor(c.r, c.g, c.b, c.a);
	glClear(GL_COLOR_BUFFER_BIT);
}

void cgml_window_poll_events(GLFWwindow *window, void (*handle_events)(GLFWwindow*)) {
	glfwPollEvents();
	if(handle_events != NULL) {
		handle_events(window);
	}
}

void cgml_window_show(GLFWwindow *window) {
	glfwSwapBuffers(window);
}

bool cgml_window_should_close(GLFWwindow *window) {
	return (
		(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) ||
		glfwWindowShouldClose(window)
	);
}

void cgml_window_loop(GLFWwindow *window, void (*draw)(GLFWwindow*), void (*poll_events)(GLFWwindow*)) {
	do {
		cgml_window_clear((color_t) {255, 255, 255, 255});

		// draw to the window
		if(draw != NULL) {
			draw(window);
		}

		cgml_window_show(window);
		cgml_window_poll_events(window, poll_events);
	} while(!cgml_window_should_close(window));
}

int cgml_window_width(GLFWwindow *window) {
	int w, h;
	glfwGetWindowSize(window, &w, &h);

	return w;
}

int cgml_window_height(GLFWwindow *window) {
	int w, h;
	glfwGetWindowSize(window, &w, &h);

	return h;
}

static void cgml_window_framebuffer_size_callback(GLFWwindow* window, int w, int h) {
	// update OpenGL draw area
	glViewport(0, 0, w, h);
}
