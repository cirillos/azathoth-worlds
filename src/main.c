#include <stdio.h>

#include "cgml/cgml.h"

int main(void) {
	GLFWwindow *window = cgml_window_create(640, 420, "CGML DEMO", NULL);

	cgml_window_loop(window, NULL, NULL);

	cgml_window_destroy(window);

	return 0;
}







/*
void draw() {
	glUseProgram(shaderProgram);

	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);
}

void cleanup() {
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, (const unsigned int*)&VBO);
	glDeleteProgram(shaderProgram);
}

void configureVAO() {
	// ------ VERTEX SHADER ------

	// vertex shader source code (right now it does not do anything, just forward the data to output)
	const char* vertexShaderSource =
		"#version 330 core\n"
		"layout (location = 0) in vec3 aPos;\n"
		"void main() {\n"
		"    gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
		"}\n";

	// create a vertexShader id
	uint64_t vertexShader = glCreateShader(GL_VERTEX_SHADER);

	// attach the vertexShader id to its source code, and then compile the shader
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);

	// check if shader was successfully compiled
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if(!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		printf("Vertex shader compilation failed: %s", infoLog);
	}

	// ------ FRAGMENT SHADER ------

	// the fragment shader is all about calculating the color output of your pixels
	const char* fragmentShaderSource =
		"#version 330 core\n"
		"out vec4 FragColor;\n"
		"void main() {\n"
		"    FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
		"}\n";

	// create fragmentShader id 
	uint64_t fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	// attach the fragmentShaderSource to the fragmentShader id, then compile the shader
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);

	// check if shader was successfully compiled
	success = 0;    
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if(!success) {
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		printf("Fragment shader compilation failed: %s", infoLog);
	}

	// ------ SHADER PROGRAM ------

	// create a shader program to link both shaders
	shaderProgram = glCreateProgram();

	// attach previously compiled shaders to shader program and link them
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	// check if linking was successful
	success = 0;    
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if(!success) {
		glGetShaderInfoLog(shaderProgram, 512, NULL, infoLog);
		printf("Shader program linking failed: %s", infoLog);
	}

	// delete the shaders, we do not need them anymore 
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void buildShaderProgram() {
	// ------ CONFIGURING VAO and VBO ------
	glGenVertexArrays(1, &VAO); // generate VAO id (stores VBO related data)
	glGenBuffers(1, &VBO);      // generate VBO id (it is a buffer that is used to send vertices to the GPU)
	
	// bind the current VBO to VAO
	glBindVertexArray(VAO);     

	// bind our VBO buffer to built-in GL_ARRAY_BUFFER type
	glBindBuffer(GL_ARRAY_BUFFER, VBO);

	// copy verticies data into the VBO buffer's memory
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// ------ LINKING VERTEX ATTRIBUTES ------

	// tel opengl how to interpret the vertex data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)(0));   
	glEnableVertexAttribArray(0);

	// once done, unbind VBO and VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

*/


















