# Sky Age (Небесный век)
It is an attempt to create an open-world 3D game. Currently only MacOS and Linux platforms are supported. Support for Windows will be added soon.

---
# Compiling Sky Age
### General dependencies
* CMake v3.1 or later
* GLFW
* OpenGL

### Linux dependencies (tested on Debian)
* If using Wayland
	* `libwayland-dev` and `wayland-protocols`
* If using Xorg
	* `xorg-dev`
* `extra-cmake-modules`

### Build process
1. Clone this repository to your machine
2. `cd` to the project's root folder (where the CMakeLists.txt resides)
3. Create a `build` folder with `mkdir build`
4. Install git submodules with `git submodule update --init --recursive`
5. Execute `./build.sh` script to build the game
6. Execute `./run.sh` script to launch the game

---
# License
All source code is licenced under GNU GPL v3 license. 
