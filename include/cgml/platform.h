#ifndef CGML_PLATFORM_H
#define CGML_PLATFORM_H

#define GL_SILENCE_DEPRECATION

// platform specific includes: GLFW, OpenGL
#if defined(_WIN32) || defined(_WIN64)
	#include <windows.h>
	#include <GL/gl.h>
	#include <GL/glu.h>
	#include <GLFW/glfw3.h>
#elif __APPLE__
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
	#include <GLUT/glut.h>
	#include <GLFW/glfw3.h>
#elif __linux__
	#include <GL/gl.h>
	#include <GL/glu.h>
	#include <GLFW/glfw3.h>
#else
	#error "Unknown compiler; cannot include libraries!"
#endif

#endif // CGML_PLATFORM_H