#ifndef CGML_WINDOW_H
#define CGML_WINDOW_H

/** WINDOW MODULE
	- cgml_window_create
	- cgml_window_destroy

	- cgml_window_clear
	- cgml_window_poll_events
	- cgml_window_show
	- cgml_window_should_close
	- cgml_window_loop
	
	- cgml_window_width
	- cgml_window_height
*/

// std includes
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// cgml includes
#include "../platform.h"
#include "color.h"

/**
Creates a new GLFW window

Params:
	w = width
	h = height
	t = title
	window_hints_init = function to initiaze GLFW window hints, if NULL is passed, uses the default init method

Returns: GLFWwindow*
*/
extern GLFWwindow *cgml_window_create(const int w, const int h, const char *const t, void (*window_hints_init)());

/*
Destroys GLFW window

Params:
	window = GLFWwindow ptr
*/
extern void cgml_window_destroy(GLFWwindow *window);

/*
Clears GLFW window screen

Params:
	window = GLFWwindow ptr
	c = color_t { r, g, b, a}
*/
extern void cgml_window_clear(const color_t c);

/*
Polls GLFW window events

Params:
	window = GLFWwindow ptr
	handle_events = function to process events
*/
extern void cgml_window_poll_events(GLFWwindow *window, void (*handle_events)(GLFWwindow*));

/*
Swap window buffers, display contents

Params:
	window = GLFWwindow ptr
*/
extern void cgml_window_show(GLFWwindow *window);

/*
Check if window close event is triggered

Params:
	window = GLFWwindow ptr

Returns: true upon close request
*/
extern bool cgml_window_should_close(GLFWwindow *window);

/*
Auto game loop

Params:
	window = GLFWwindow ptr
	draw = draw function 
	poll_events = process events function

Returns: true upon close request
*/
extern void cgml_window_loop(GLFWwindow *window, void (*draw)(GLFWwindow*), void (*poll_events)(GLFWwindow*));

/*
Get GLFW window width/height

Params:
	window = GLFWwindow ptr

Returns: int
*/
extern int cgml_window_width(GLFWwindow *window);
extern int cgml_window_height(GLFWwindow *window);

#endif // CGML_WINDOW_H




























